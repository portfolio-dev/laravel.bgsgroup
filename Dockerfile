# Dockerfile позволяет создавать персонализированные образы, которые можно использовать для установки требуемого
# программного обеспечения. Этот файл будет задавать базовый образ и необходимые команды и инструкции для построения образа приложения.

FROM php:7.4-fpm

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Install system dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    mysql-client \
    openssl \
    libonig-dev \
    zip \
    unzip \
    git \
    curl

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql exif pcntl

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u $uid -d /home/$user/programming/projects/php/laravel7 $user
RUN mkdir -p /home/$user/programming/projects/php/laravel7/.composer && \
    chown -R $user:$user /home/$user/programming/projects/php/laravel7

# Set working directory
WORKDIR /var/www

USER $user
