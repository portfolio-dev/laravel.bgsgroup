<?php

namespace Tests\Unit;

use Tests\TestCase;

class CustomersUnitTest extends TestCase
{
    /**  @test */
    public function testCreateCustomerWithMiddleware()
    {
        $data = [
            'first_name' => "New Name Customer",
            'last_name' => "Last Name Customer",
            'email' => "test@localhost.ru"
        ];

        $response = $this->json('POST', '/api/customers', $data);
        $response->assertStatus(401);
        $response->assertJson(['message' => "Unauthenticated."]);
    }

    /**  @test */
    public function testGetAllCustomers()
    {
        $response = $this->json('GET', '/api/customers');
        $response->assertStatus(200);

        $response->assertJsonStructure(
            [
                [
                    'id',
                    'first_name',
                    'last_name',
                    'email',
                    'created_at',
                    'updated_at'
                ]
            ]
        );
    }
}
