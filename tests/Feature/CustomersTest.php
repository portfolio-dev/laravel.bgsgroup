<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CustomersTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function testViewCustomer()
    {
        $customer = factory(\App\Customer::class)->create();

        $response = $this->get('/api/customers');

        $response->assertSee($customer->email);
    }
}
