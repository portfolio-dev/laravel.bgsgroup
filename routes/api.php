<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API'], function() {
    Route::apiResource('customers', 'CustomerController');

    Route::group(['prefix' => 'customers'], function() {
        Route::get('/{customer}/activities', [
            'uses' => 'CustomerController@activities',
            'as' => 'customers.activities',
        ]);

        Route::get('/{customer}/activities/{activity}', [
            'uses' => 'CustomerController@activity',
            'as' => 'customers.activity.details',
        ]);
    });

    Route::group(['prefix' => 'activities'], function() {
        Route::get('/{activity}/customers', [
            'uses' => 'ActivityController@customers',
            'as' => 'activity.customers',
        ]);
    });
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
