<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Activity;
use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ActivityController extends Controller
{
    /** @var Activity $activity */
    protected $activity;

    /**
     * @param Activity $activity
     */
    public function __construct(Activity $activity)
    {
        $this->activity = $activity;
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function customers(int $id)
    {
        /** @var  Customer $customers */
        $customers = Customer::whereHas('activities', $filter = function($query)  use ($id) {
            $query->where('id', '=', $id);
        })
        ->with(['activities' => $filter])
        ->get();

        return response()->json([
            'error' => false,
            'customers' => $customers,
        ], Response::HTTP_OK);
    }
}
