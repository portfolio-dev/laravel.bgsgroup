<?php

declare(strict_types=1);

namespace App\Http\Traits;

use Illuminate\Http\Response;

trait CustomerTrait
{
    public function notFound()
    {
        return response()->json([
            'error' => true,
            'message' => 'Неудалось найти',
        ], Response::HTTP_NOT_FOUND);
    }
}
