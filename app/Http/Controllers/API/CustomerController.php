<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Activity;
use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\Http\Traits\CustomerTrait;
use App\Jobs\SendEmail;
use Illuminate\Http\Response;

class CustomerController extends Controller
{
    use CustomerTrait;

    /** @var Customer  $customer */
    protected $customer;

    /**
     * @param Customer $customer
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        /** @var  Customer[] $customers */
        $customers = $this->customer->paginate(10);

        if (is_null($customers)) {
            return $this->notFound();
        }

        return response()->json([
            'error' => false,
            'customers' => $customers,
        ], Response::HTTP_OK);
    }

    /**
     * @param CustomerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CustomerRequest $request)
    {
        /** @var  Customer $customer */
        $customer = $this->customer->create($request->validated());

        SendEmail::dispatch(['email' => 'test']);

        return response()->json([
            'error' => false,
            'customer' => $customer,
        ], Response::HTTP_CREATED);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id)
    {
        /** @var  Customer $customer */
        $customer = $this->customer->getByIdWithActivity($id);

        if (is_null($customer)) {
            return $this->notFound();
        }

        return response()->json([
            'error' => false,
            'customer' => $customer,
        ], Response::HTTP_OK);
    }

    /**
     * @param  CustomerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CustomerRequest $request, int $id)
    {
        /** @var  Customer $customer */
        $customer = $this->customer->getById($id);

        if (is_null($customer)) {
            return $this->notFound();
        }

        $customer->edit($request->all());

        return response()->json([
            'error' => false,
            'customer' => $customer,
        ], Response::HTTP_OK);
    }

    /**
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        /** @var Customer  $customer */
        $customer = $this->customer->getById($id);

        if (is_null($customer)) {
            return $this->notFound();
        }

        $customer->remove();

        return response()->json([
            'error' => false,
            'message' => "Пользователь $customer->id успешно удален",
        ], Response::HTTP_OK);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function activities(int $id)
    {
        /** @var  Customer $customer */
        $customer = $this->customer->getById($id);

        if (is_null($customer)) {
            return $this->notFound();
        }

        /** @var  Activity[] $activities */
        $activities = $customer->getAllActivities();

        return response()->json([
            'error' => false,
            'activities' => $activities,
        ], Response::HTTP_OK);
    }

    /**
     * @param int $id
     * @param int $id_activity
     * @return \Illuminate\Http\JsonResponse
     */
    public function activity(int $id, int $id_activity)
    {
        /** @var  Customer $customer */
        $customer = $this->customer->getById($id);

        if (is_null($customer)) {
            return $this->notFound();
        }

        /** @var  Activity $activity */
        $activity = $customer->getActivityById($id_activity);

        return response()->json([
            'error' => false,
            'activity' => $activity,
        ], Response::HTTP_OK);
    }
}
