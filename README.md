## API bgsgroup

Приложение для управления участниками мероприятия

### Установка
1. Склонировать проект **laravel.bgsgroup.git**: `git clone https://businesstep@bitbucket.org/businesstep/laravel.bgsgroup.git`
2. Перейти в проект: `cp laravel.bgsgroup`
3. Файл .env.example скопировать и переименовать в .env. В нем указать доступы к БД

### Запуск 
 - #### с помощью Docker
 
    Запустить docker-up.sh

- #### или без Docker

	1. Установить Composer зависимости: `php composer install`
	2. Сгенерировать ключ: `php artisan key:generate`
	3. Запустить в БД миграции: `php artisan migrate`
	4. Запустить заполнение данными БД: `php artisan db:seed --class=DatabaseSeeder`
	5. Запустить сервер: `php artisan serve --port=8000`
	6. С помощью Postman проверить работу API

### Проверка API
  headers
	
	`Content-Type: application/x-www-form-urlencoded`
	
	`Accept: application/json`
	
1. Список всех участников http://localhost:8000/api/customers - get
2. Информация по конкретному участнику http://localhost:8000/api/customers/{id} - get
3. Создание участника http://localhost:8000/api/customers - post
    
	<small>параметры</small>
    `first_name`
	`last_name`
	`email`
	
4. Редактирование участника http://localhost:8000/api/customers/{id} - update
    
	<small>параметры</small>
    `first_name`
	`last_name`
	`email`

5. Удаление участника http://localhost:8000/api/customers/{id} - delete    

6. Фильтрация данных при запросе(участники определенного мероприятия) http://localhost:8000/api/activities/{id}/customers

### Unit тестирование
запустить в консоли ./vendor/bin/phpunit
