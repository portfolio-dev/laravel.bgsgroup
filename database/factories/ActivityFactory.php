<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Activity;
use Faker\Generator as Faker;

$factory->define(Activity::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'customer_id' => function() {
            return \App\Customer::all()->random();
        },
        'activity_date' => now(),
        'address' => $faker->address,
    ];
});
