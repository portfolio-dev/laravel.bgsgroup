<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @package App
 */
class Customer extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'first_name',
        'last_name',
        'email'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activities()
    {
        return $this->hasMany(Activity::class)->orderBy('id');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getByIdWithActivity(int $id)
    {
        return $this->byId($id)->with('activities')->first();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function  getById(int $id)
    {
        return $this->byId($id)->first();
    }

    /**
     * @param $query
     * @param int $id
     * @return mixed
     */
    public function scopeById($query, int $id)
    {
        return $query->where('id', $id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAllActivities()
    {
        return $this->activities()->get();
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection|Model|\Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Relations\HasMany[]|null
     */
    public function getActivityById(int $id)
    {
        return $this->activities()->find($id);
    }

    /**
     * @param array $fields
     */
    public function edit(array $fields): void
    {
        $this->fill($fields);
        $this->save();
    }

    /**
     * @throws \Exception
     */
    public function remove(): void
    {
        $this->delete();
    }
}
